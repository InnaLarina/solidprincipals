﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_BubbleSorting.Array
{
    class ArrayOperations: IPrintArray, IIniArray
    {
        protected const int lenthArray = 10;
        const int maxArray = 20;
        protected int[] ourArray;
        protected int[] sortedArray;
        protected Random random;
        int[] OurArray { get;}
        int[] SortedArray { get; set; }

        public void IniArray()
        {
            //инициализация массива
            for (int i = 0; i < lenthArray; i++)
            {
                ourArray[i] = random.Next(0, maxArray);
            }
        }
        public void PrintArray(string text, int[] mas)
        {
            //печать массива
            for (int i = 0; i < lenthArray; i++)
            {
                if (i == 0) Console.WriteLine(text);
                if (i == lenthArray - 1)
                    Console.Write(ourArray[i]);
                else Console.Write(ourArray[i] + ",");
            }
            Console.WriteLine();
        }
        
    }
}

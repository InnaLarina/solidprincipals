﻿using System;
using SOLID_BubbleSorting.Array;

namespace SOLID_BubbleSorting.Array
{
    class InverseSorting:IBubbleSortArray
    {
        public int[] BubbleSort(int[] mas)
        {
            // сортировка
            int temp;
            for (int i = 0; i < mas.Length; i++)
            {
                for (int j = i + 1; j < mas.Length; j++)
                {
                    if (mas[i] < mas[j])
                    {
                        temp = mas[i];
                        mas[i] = mas[j];
                        mas[j] = temp;
                    }
                }
            }
            return mas;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_BubbleSorting.Array
{
    interface IPrintArray
    {
        void PrintArray(string text, int[] mas);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOLID_BubbleSorting.Array
{
    class ManyEights:IBubbleSortArray
    {
        public int[] BubbleSort(int[] mas)
        {
            // массив состоит только из восьмерок
            for (int i = 0; i < mas.Length; i++)
            {
                mas[i] = 8;
            }
            return mas;
        }
    }
}

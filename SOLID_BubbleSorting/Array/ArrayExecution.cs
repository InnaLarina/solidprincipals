﻿using System;

namespace SOLID_BubbleSorting.Array
{
    class ArrayExecution: ArrayOperations
    {
        IBubbleSortArray bubbleSorting;
        public int[] OurArray 
        {
            get 
            { 
                return ourArray; 
            }
        }
        
        public int[] SortedArray
        {
            get
            {
                return sortedArray;
            }
            set 
            {
                sortedArray = value;
            }
        }
        

        public ArrayExecution(IBubbleSortArray bubbleSorting)
        {
            this.ourArray = new int[lenthArray];
            this.random = new Random();
            this.bubbleSorting = bubbleSorting;
        }
        public void Execute() 
        {
            IniArray();
            PrintArray("Массив: ",OurArray);
            SortedArray = bubbleSorting.BubbleSort(OurArray);
            PrintArray("Отсортированный массив: ", SortedArray);
            Console.ReadKey();
        }
    }
}

﻿using System;
using SOLID_BubbleSorting.Array;

namespace SOLID_BubbleSorting
{
    class Program
    {
        
        static void Main(string[] args)
        {
            IBubbleSortArray inversesorting = new InverseSorting();
            ArrayExecution arrExec = new ArrayExecution(inversesorting);
            arrExec.Execute();
            
        }
        
    }
}

Проект для демонстрации принципов Solid

Сортирует массив пузырьковой сортировкой.

Принципы SOLID реализованы в:

1. Принцип единственной ответственности: Инизиализация массива, Печать и Сортировка реализована отдельными методами:IniArray,PrintArray,BubbleSort.

2. Принцип инверсии зависимостей: Реализация методов может быть любой, главное реализовать сигнатуру методов интерфейса.

3. Принцип разделения интерфейса: методы IniArray,PrintArray,BubbleSort реализованы в соответствующих интерфейсах, содержащих единственно соответcтвующие методы.

4. Принцип открытости/закрытости: классы InverseSorting, RightSorting, ManyEights по-разному реализуют интерфейс IBubbleSortArray

5. Принцип подстановки Барбары Лисков:метод ManyEights вообше не сортирует массив, а его подменяет восьмерками, он только реализуют интерфейс IBubbleSortArray, а не перекрывает его сортировочный метод.